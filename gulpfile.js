'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('watch', function () {
	
	browserSync.init({
		proxy: "https://wp.test",
		injectChanges: true,
		https: true,
		snippetOptions: {
			ignorePaths: "wp-admin/**"
		}

	});
	
	gulp.watch(`**/*.php`).on('change', browserSync.reload);
	
});S