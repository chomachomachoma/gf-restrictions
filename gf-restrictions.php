<?php
/**
 * @wordpress-plugin
 * Plugin Name:       GF Restrictions
 * Description:       This is a WordPress plugin template to jumpstart plugin development.
 * Version:           0.0.1
 * Author:            Christopher Choma
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gf-restrictions
 */

// If this file is called directly, abort.

if ( ! defined( 'WPINC' ) ) {
	die;
}

// Define plugin version
define( 'GF_RESTRICTIONS_VERSION', '1.0' );

function gf_restrictions_get_roles () {
	
	global $wp_roles;
	
	$roles = array();
	
	foreach ( $wp_roles->get_names() as $key=>$value ) {
		$roles[] = 
			array(
				'name'  => 'limitRole_' . $key,
				'label' => __( $value, 'gravityforms' ),
		);
	}
	
	return $roles;
	
}

function gf_restrictions_current_user_roles() {
	
	if( is_user_logged_in() ) {
		$user = wp_get_current_user();
		$roles = ( array ) $user->roles;
		return $roles;
	} else {
		return array();
	}
	
}

/* 
Append restrictions settings fields to $field array using field framework.
*/
add_filter( 'gform_form_settings_fields', 'gf_restrictions_add_fields', 10, 2 );
function gf_restrictions_add_fields ($fields, $form) {
	
	global $__gf_tooltips;
	
	$__gf_tooltips['form_limit_users'] = 'Restrict access to permitted user ids. Logically, the user must also be logged in to get the user id.';
	$__gf_tooltips['form_limit_roles'] = 'Restrict access to permitted user role(s). Logically, the user must also be logged in to get the user role.';
	
	$registered_roles = gf_restrictions_get_roles();
	
	$fields['restrictions']['fields'][] = array(
		'name'    => 'limitUsers',
		'type'    => 'checkbox',
		'label'   => __( 'Limit to specific users', 'gravityforms' ),
		'tooltip' => gform_tooltip( 'form_limit_users', '', true ),
		'choices' => array(
			array(
				'name'  => 'limitUsers',
				'label' => __( 'Enable specific users', 'gravityforms' ),
			),
		),
		'fields'  => array(
			array(
				'name'    => 'limitUsersIDs',
				'type'    => 'text',
				'label'   => __( 'Comma separated list of user ids permitted to view the form', 'gravityforms' ),
				'dependency' => array(
					'live'   => true,
					'fields' => array(
						array(
							'field' => 'limitUsers',
						),
					),
				),
			),
			array(
				'name'    => 'limitUsersMessage',
				'type'    => 'textarea',
				'label'   => __( 'Message to show prohibited users', 'gravityforms' ),
				'dependency' => array(
					'live'   => true,
					'fields' => array(
						array(
							'field' => 'limitUsers',
						),
					),
				),
			),
		),
	);
	
	$fields['restrictions']['fields'][] = array( 
		'name'    => 'limitRoles',
		'type'    => 'checkbox',
		'label'   => __( 'Limit to specific roles', 'gravityforms' ),
		'tooltip' => gform_tooltip( 'form_limit_roles', '', true ),
		'choices' => array(
			array(
				'name'  => 'limitRoles',
				'label' => __( 'Enable specific roles', 'gravityforms' ),
			),
		),
		'fields'  => array(
			array(
				'name'    => 'limitRolesCheckboxes',
				'type'    => 'checkbox',
				'label'   => __( 'Roles permitted to view the form', 'gravityforms' ),
				'choices' => $registered_roles,
				'dependency' => array(
					'live'   => true,
					'fields' => array(
						array(
							'field' => 'limitRoles',
						),
					),
				),
			),
			array(
				'name'    => 'limitRolesMessage',
				'type'    => 'textarea',
				'label'   => __( 'Message to show prohibited users', 'gravityforms' ),
				'dependency' => array(
					'live'   => true,
					'fields' => array(
						array(
							'field' => 'limitRoles',
						),
					),
				),
			),
		),
	);
  
	return $fields;
	
}

/* 
Check current logged in user permissions against settings.
*/
add_filter( 'gform_get_form_filter', 'gf_restrictions_check_user', 10, 2 );
function gf_restrictions_check_user( $form_string, $form ) {
	
	$current_user_id = get_current_user_id();
	$permitted_user_ids = array_key_exists('limitUsersIDs', $form) ? explode(',', $form['limitUsersIDs']) : array();
	
	if ( array_key_exists('limitUsers', $form) && $form['limitUsers'] == 1 && !in_array($current_user_id, $permitted_user_ids) ) {
		$form_string = empty( $form['limitUsersMessage'] ) ? '<p>' . esc_html__( 'Sorry. This form has been restricted to permitted users only.', 'gravityforms' ) . '</p>' : '<p>' . GFCommon::gform_do_shortcode( $form['limitUsersMessage'] ) . '</p>';
	}
	
	if ( array_key_exists('limitRoles', $form) && $form['limitRoles'] == 1 ) {
		
		$current_user_roles = gf_restrictions_current_user_roles();
		$registered_roles = gf_restrictions_get_roles();
		$allowed_roles = array();
		$allowed = false;
		
		// Loop through registered roles to check against roles allowed to access the form. Builds an array of allowed roles based on settings.
		foreach ( $registered_roles as $registered_role ) {
			if (array_key_exists($registered_role['name'], $form)) {
				if ( $form[$registered_role['name']] == 1 ) {
					$allowed_roles[] = str_replace('limitRole_', '', $registered_role['name']);
				}
			}
		}
		
		// Loop through the current users' roles to check if they are on list of allowed roles. Set allowed flag to true.
		foreach ( $current_user_roles as $current_user_role ) {
			if ( in_array($current_user_role, $allowed_roles) ) {
				$allowed = true;
			}
		}
		
		// If not allowed, modify the form string.
		if ( $allowed != true ) {
			$form_string = empty( $form['limitRolesMessage'] ) ? '<p>' . esc_html__( 'Sorry. This form has been restricted to permitted roles only.', 'gravityforms' ) . '</p>' : '<p>' . GFCommon::gform_do_shortcode( $form['limitRolesMessage'] ) . '</p>';
		}
		
	}
	
	return $form_string;
	
}

?>